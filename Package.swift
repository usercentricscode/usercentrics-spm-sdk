// swift-tools-version:5.3
import PackageDescription

let package = Package(
    name: "Usercentrics",
    platforms: [
        .iOS(.v11),
        .tvOS(.v11)
    ],
    products: [
        .library(
            name: "Usercentrics",
            targets: ["Usercentrics"]
        ),
    ],
    targets: [
        .binaryTarget(
            name: "Usercentrics",
            url: "https://bitbucket.org/usercentricscode/usercentrics-spm-sdk/downloads/Usercentrics-2.18.9.xcframework.zip",
            checksum: "0032248a862b34119f2bc250ea706314d5e85d1f92231f0422effa156a8b71aa"
        ),
    ]
)
